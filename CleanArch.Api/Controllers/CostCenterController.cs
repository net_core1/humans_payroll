﻿using Application.Cqrs.Mekano.Queries;
using Application.Cqrs.ResponsabilitiesCostCenter.Commands;
using Application.Cqrs.ResponsabilitiesCostCenter.Queries;
using ClaroFidelizacion.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Humans.Payroll.Api.Controllers
{
    [Route("api/costcenter")]
    [ApiController]
    //[Authorize]
    public class CostCenterController : ApiControllerBase
    {
        /// <summary>
        /// Método que retorna todos los Centros de costos existentes en el sistema.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCostCenter([FromQuery] GetCostCenterQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        /// <summary>
        /// Metodo que retorna el Centro de Costos y el Status  por el Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByCostCenterId(int id)
        {
            return Ok(await Mediator.Send(new GetCostCenterByIdQuery() { Id = id }));
        }

        /// <summary>
        /// Crea Responsable para el Centro de costos
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]     
        public async Task<IActionResult> Post([FromBody] PostResponsabilityCostCenterCommand query)
        {
            return Ok(await Mediator.Send(query));
        }

        /// <summary>
        /// Retorna el responsable del Centro de Costos para las horas extras.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("responsabilities-costcenter")]
        public async Task<IActionResult> Get([FromQuery] GetResponsabilityCostCenterByDocQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

    }
}
