﻿using Application.Cqrs.OvertimeTypes.Queries;
using ClaroFidelizacion.Api.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Humans.Payroll.Api.Controllers
{
    public class OvertimeTypeController : ApiControllerBase
    {


        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] GetOvertimeTypesQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

    }
}
