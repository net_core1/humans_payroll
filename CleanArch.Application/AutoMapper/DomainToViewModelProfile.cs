﻿using Application.AutoMapper.Resolver;
using Application.DTOs.Configurations;
using Application.DTOs.Mekano;
using Application.DTOs.OvertimePeriods;
using Application.DTOs.User;
using AutoMapper;
using Core.Models.configuration;
using Domain.Models.Mekano;
using Domain.Models.Overtimes;
using Domain.Models.Rol;
using Domain.Models.User;

namespace CleanArch.Application.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {

            #region Users-Rols-UserRols  
            
            CreateMap<User, UserDto>();
            CreateMap<UserRol, UserRolDto>();
            CreateMap<Rol, RolDto>();

            #endregion

            #region Configuration
            CreateMap<Configuration, ConfigurationDto>();
            CreateMap<Category, CategoryDto>();
            #endregion

            #region Overtimes
            
            CreateMap<OvertimePeriod, OvertimePeriodDto>()
                 .ForMember(x => x.StartAllowedDate, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.StartAllowedDate)))
                 .ForMember(x => x.EndAllowedDate, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.EndAllowedDate)))
                 .ForMember(x => x.EndPeriodDate, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.EndPeriodDate)))
                 .ForMember(x => x.StartPeriodDate, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.StartPeriodDate)));
            CreateMap<OvertimeDetail, OvertimeDetailDto>()
                .ForMember(x => x.OvertimeDay, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.OvertimeDay)));
            CreateMap<OvertimeType, OvertimeTypeDto>();

            #endregion

            #region CostCenter          
            CreateMap<CostCenter, CostCenterDto>();
            CreateMap<ResponsabilityCostCenter, ResponsabilityCostCenterDto>();
            #endregion
        }
    }
}