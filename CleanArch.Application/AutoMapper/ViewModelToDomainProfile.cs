﻿using Application.Cqrs.OvertimePeriodDetail.Commands;
using Application.Cqrs.OvertimePeriods.Commands;
using Application.Cqrs.ResponsabilitiesCostCenter.Commands;
using Application.Cqrs.Rol.Commands;
using Application.Cqrs.User.Commands;
using Application.Cqrs.UserRol.Commands;
using Application.DTOs.Configurations;
using Application.DTOs.Mekano;
using Application.DTOs.OvertimePeriods;
using Application.DTOs.User;
using AutoMapper;
using Core.Models.configuration;
using Domain.Models.Mekano;
using Domain.Models.Overtimes;
using Domain.Models.Rol;
using Domain.Models.User;

namespace CleanArch.Application.AutoMapper
{
    public class ViewModelToDomainProfile : Profile
    {
        public ViewModelToDomainProfile()
        {


            CreateMap<UploadOvertimesPeriodCommand, OvertimePeriod>();
            CreateMap<CreateOvertimePeriodcommand, OvertimePeriod>();
            CreateMap<UpdateOvertimePeriodCommand, OvertimePeriod>();

            CreateMap<DeleteOvertimeDetailCommand, OvertimeDetail>();
            CreateMap<PostOvertimeCommand, OvertimeDetail>();
            CreateMap<PutOvertimeCommand, OvertimeDetail>();

            CreateMap<OvertimeDetailDto, OvertimeDetail>();


            #region Users-Rols-UserRols

            // Users
            CreateMap<UserDto, User>();
            CreateMap<PostUserCommand, User>();
            CreateMap<PutUserCommand, User>();
            CreateMap<DeleteUserCommand, User>();
            CreateMap<PutStatusActivateUserById, User>();
            CreateMap<PutStatusDeactivateUserById, User>();

            // Rols
            CreateMap<RolDto, Rol>();
            CreateMap<PostRolCommand, Rol>();
            CreateMap<PutRolCommand, Rol>();
            CreateMap<DeleteRolCommand, Rol>();
            CreateMap<PutStatusActivateRolById, Rol>();
            CreateMap<PutStatusDeactivateRolById, Rol>();

            // UserRol
            CreateMap<UserRolDto, UserRol>();
            CreateMap<AddUserRolCommand, UserRol>();
            CreateMap<PutUserCommand, UserRol>();
            CreateMap<DeleteUserRolCommand, UserRol>();
            CreateMap<PutUserCommand, UserRol>();

            #endregion

            #region Configurations          
            CreateMap<CategoryDto, Category>();
            CreateMap<ConfigurationDto, Configuration>();
            #endregion

            #region ResponsabilityCostCenter
            CreateMap<PostResponsabilityCostCenterCommand, ResponsabilityCostCenter>();
            CreateMap<ResponsabilityCostCenterDto, ResponsabilityCostCenter>();
            #endregion

        }
    }
}

