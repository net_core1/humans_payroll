﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Cqrs.OvertimePeriods.Commands
{
    public class UploadOvertimesPeriodCommand : IRequest<List<Dictionary<string, object>>>
    {
        public IFormFile File { get; set; }
        public int OvertimePeriodId { get; set; }


    }

    public class UploadOvertimesPeriodCommandHandle : IRequestHandler<UploadOvertimesPeriodCommand, List<Dictionary<string, object>>>
    {


        private readonly IBulkInsert _bulkInsert;
        private readonly IMapper _mapper;
        public UploadOvertimesPeriodCommandHandle(
            IBulkInsert bulkInsert,
            IMapper mapper
        )
        {
            _mapper = mapper;
            _bulkInsert = bulkInsert;

        }
        public async Task<List<Dictionary<string, object>>> Handle(UploadOvertimesPeriodCommand request, CancellationToken cancellationToken)
        {
            var response = await _bulkInsert.SaveFile(request);

            if (response.Result)
            {
                _bulkInsert.setTemporalTable("OvertimeTemps");
                var Resultdt = await _bulkInsert.Bulk(response.Data);
                var errors = GetErrorList(Resultdt);
                return errors.Count > 0 ? errors : GetErrorList(await _bulkInsert.ExecuteStore("P_InsertOvertimesTemps"));
            }
            else
            {
                DataTable dt = new DataTable("Errors");
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;        
              
                DataColumn column1 = new DataColumn("Message");                
                dt.Columns.Add(column1);               
                DataRow row1 = dt.NewRow();
                row1["Message"] = response.Message;              
                dt.Rows.Add(row1);

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, response.Message);
                    }
                    rows.Add(row);
                }

                return rows;
            }

        }

        public List<Dictionary<string, object>> GetErrorList(DataTable result)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in result.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in result.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }

            return rows;

        }

    }
}
