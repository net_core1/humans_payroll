﻿using Application.Interfaces.User;
using FluentValidation;

namespace Application.Cqrs.User.Commands
{
    public class PutUserCommandValidator : AbstractValidator<PutUserCommand>
    {
        private readonly IUserService _userService;

        public PutUserCommandValidator(
            IUserService userService
            )
        {
            _userService = userService;

            RuleFor(x => x.Document)
                    .NotEmpty()
                    .NotNull()
                    .WithMessage("El documento no puede estar vacío");

            RuleFor(x => x.Names)
                    .NotEmpty()
                    .NotNull()
                    .WithMessage("El nombre no puede estar vacío"); 

            RuleFor(x => x.PassWord)
                   .NotEmpty()
                   .WithMessage("El password no puede estar vacía");

        

        }

    }
}
