﻿using System.Threading.Tasks;

namespace Application.Interfaces.Mekano
{
    public interface IMekanoUserService
    {
        Task<bool> checkByDocumentExists(string document);
    }
}
