﻿using Application.Cqrs.OvertimePeriods.Commands;
using Application.Cqrs.OvertimePeriods.Queries.GetPeriods;
using Application.DTOs.OvertimePeriods;
using Domain.CustomEntities;
using Domain.Models.Overtimes;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Interfaces.Overtimes
{
    public interface IOvertimePeriodService
    {
        Task<PaginatedList<OvertimePeriodDto>> GetPeriods(GetOvertimePeriodQuery request, CancellationToken cancellationToken);
        Task<OvertimePeriod> PostPeriod(OvertimePeriod overtimePeriod, CancellationToken cancellationToken);
        Task<OvertimePeriod> GetById(int Id);
        Task<OvertimePeriod> PutPeriod(UpdateOvertimePeriodCommand request);
        Task<bool> checkById(int Id);
    }
}
