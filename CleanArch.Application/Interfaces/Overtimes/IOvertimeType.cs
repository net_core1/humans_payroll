﻿using Domain.Models.Overtimes;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Interfaces.Overtimes
{
    public interface IOvertimeTypeService
    {
        Task<IList<OvertimeType>> Get(CancellationToken cancellationToken);
        Task<bool> checkById(Guid id);
        Task<string> GetOvertimeTypeNameById(Guid id);

    }
}
