﻿using Application.Cqrs.User.Commands;
using Application.DTOs.User;
using Domain.Models.Mekano;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.User
{
    public interface IUserService
    {
        Task<List<UserDto>> GetUser(int without);
        Task<bool> CheckUserExistsByDocument(string document);
        Task<MekanoUser> GetUserByDocumentInMekano(string document);
        Task<bool> CheckUserExistsByDocumentInMekano(string document); 
        Task<string> GetCampaignNameFromMekano(string document); 
        UserDto GetUserByDocumentAndSubcampaign(string document, int subCampaignId);
        Domain.Models.User.User PutUserExcel(UserDto userDto);       
        Task<UserDto> PostUser(Domain.Models.User.User user);
        Task<UserDto> PutStatusActivateUserById(PutStatusActivateUserById request);
        Task<UserDto> PutStatusDeactivateUserById(PutStatusDeactivateUserById request);
        Task<UserDto> PutUser(PutUserCommand request);
        Task<bool> DeleteUser(Guid id);
        bool CheckById(Guid? Id);        
       
    }
}
