﻿using Application.Interfaces.Mekano;
using AutoMapper;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Mekano
{
    public class MekanoUserService : IMekanoUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public MekanoUserService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<MekanoUserService> logger) => (_unitOfWork, _mapper, _logger) = (unitOfWork, mapper, logger);

        public async Task<bool> checkByDocumentExists(string document)
        {
            return await _unitOfWork.MekanoUserRepository.Get()
                             .Where(x => x.Document.Trim() == document.Trim())
                             .CountAsync() > 0 ? true : false;

        }
    }
}
