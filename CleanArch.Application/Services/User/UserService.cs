﻿using Application.Core.Exceptions;
using Application.Cqrs.User.Commands;
using Application.DTOs.User;
using Application.Interfaces.User;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces;
using Domain.Interfaces.User;
using Domain.Models.Mekano;
using Domain.Models.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRolService _userRolService;
        private readonly IMapper _autoMapper;
        private readonly ICurrentUserService _currentUser;
        private readonly ILogger _logger;

        private Guid? CreatedBy = null;
        private Guid? UpdatedBy = null;
        public UserService(
            IUserRepository userRepository,
            IUserRolService userRolService,
            ICurrentUserService currentUser,
            IUnitOfWork unitOfWork,
            IMapper autoMapper,
            ILogger<UserService> logger

            )
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _userRolService = userRolService;
            _currentUser = currentUser;
            _autoMapper = autoMapper;
            CreatedBy = _currentUser.GetUserInfo().Id;
            UpdatedBy = _currentUser.GetUserInfo().Id;
            _logger = logger;


        }
        public async Task<List<UserDto>> GetUser(int without)
        {
            var source = _unitOfWork.UserRepository.Get();

            return without == 1 ? await source.ProjectTo<UserDto>(_autoMapper.ConfigurationProvider).ToListAsync()
                                : await source.Where(x => x.Status).ProjectTo<UserDto>(_autoMapper.ConfigurationProvider).ToListAsync();

        }
        public UserDto GetUserByDocumentAndSubcampaign(string document, int subCampaignId)
        {
            UserDto users = new UserDto();

            users = _userRepository
                      .Get()
                      .Where(c => c.Document == document.Trim())
                      .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                      .FirstOrDefault();
            return users;

        }
        public bool CheckById(Guid? Id)
        {
            return _userRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }
        public async Task<UserDto> PostUser(Domain.Models.User.User user)
        {
            try
            {
                user.CreatedBy = CreatedBy;
                return _autoMapper.Map<UserDto>(await _unitOfWork.UserRepository.Add(user));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al crear el registro, UserService en el método PostUser, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }


        }
        public async Task<UserDto> PutStatusActivateUserById(PutStatusActivateUserById request)
        {
            var user = await GetUserById(request.Id) ?? throw new NotFoundException("El registro a procesar no existe");
            try
            {
                user = _autoMapper.Map<PutStatusActivateUserById, Domain.Models.User.User>(request, user);
                user.UpdatedBy = UpdatedBy;
                user.UpdatedAt = DateTime.Now;
                return _autoMapper.Map<UserDto>(await _userRepository.PutStatusActivateUserById(user));
            }
            catch (Exception ex)
            {

                _logger.LogError($"Error al activar el usuario, UserService en el método PutStatusDeactivateUserById, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }

           
        }
        public async Task<UserDto> PutStatusDeactivateUserById(PutStatusDeactivateUserById request)
        {

            var user = await GetUserById(request.Id) ?? throw new NotFoundException("El registro a procesar no existe");
            try
            {
                user = _autoMapper.Map<PutStatusDeactivateUserById, Domain.Models.User.User>(request, user);
                user.UpdatedBy = CreatedBy;
                user.UpdatedAt = DateTime.Now;
                return _autoMapper.Map<UserDto>(await _userRepository.PutStatusDeactivateUserById(user));
            }
            catch (Exception ex)
            {

                _logger.LogError($"Error al inactivar el usuario, UserService en el método PutStatusDeactivateUserById, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }
         

        }
        public async Task<UserDto> PutUser(PutUserCommand request)
        {
            var user = await GetUserById(request.Id) ?? throw new NotFoundException("El registro a procesar no existe");
            DeleteUserRolRange(user.Id);
            PostUserRolRange(request);
            return await UpdateUser(user, request);

        }
        public Domain.Models.User.User PutUserExcel(UserDto userDto)
        {
            var user = _autoMapper.Map<Domain.Models.User.User>(userDto);
            _userRepository.Put(user);

            return user;
        }
        public async Task<bool> DeleteUser(Guid id)
        {
            var user = await GetUserById(id) ?? throw new NotFoundException("El registro a procesar no existe");

            try
            {
                DeleteUserRolRange(user.Id);
                return await _unitOfWork.UserRepository.Delete(await GetUserById(id));

            }
            catch (Exception ex)
            {

                _logger.LogError($"Error al eliminar el registro, UserService en el método DeleteUser, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }
        }
        private async Task<Domain.Models.User.User> GetUserById(Guid id)
        {
            return await _unitOfWork.UserRepository.GetById(id) ?? throw new NotFoundException("La entidad a procesar no existe");
        }
        public async Task<bool> CheckUserExistsByDocument(string document)
        {

            return await _unitOfWork.UserRepository.Get()
                                                   .Where(x => x.Document.Trim()
                                                   .Equals(document.Trim()))
                                                   .CountAsync() > 0 ? false : true; ;

        }
        public async Task<bool> CheckUserExistsByDocumentInMekano(string document)
        {
            return await _userRepository.GetUsersMekano().Where(x => x.Document.Trim() == document.Trim() && x.Active == true)
                                                         .CountAsync() > 0 ? true : false;


        }
        public async Task<string> GetCampaignNameFromMekano(string document)
        {

            return await _userRepository.GetUsersMekano()
                                              .Where(x => x.Document.Trim() == document.Trim() && x.Active == true)
                                              .Select(x => x.Campaign)
                                              .FirstOrDefaultAsync();


        }
        private void PostUserRolRange(PutUserCommand request)
        {
            try
            {
                var ListUserRol = new List<UserRol>() { };

                foreach (var rol in request.Roles)
                {
                    ListUserRol.Add(new UserRol()
                    {
                        RolId = rol,
                        UserId = request.Id
                    });
                }

                if (ListUserRol.Count > 0) _userRolService.PostRange(ListUserRol);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al insertar listado de roles, UserService en el método PostUserRolRange, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }
        }
        private void DeleteUserRolRange(Guid id)
        {
            try
            {
                _userRolService.DeleteRangeByUserId(id);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al eliminar listado de roles, UserService en el método DeleteUserRolRange, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }
        }
        private async Task<UserDto> UpdateUser(Domain.Models.User.User user, PutUserCommand request)
        {
            try
            {
                user = _autoMapper.Map<PutUserCommand, Domain.Models.User.User>(request, user);
                user.UpdatedBy = CreatedBy;
                user.UpdatedAt = DateTime.Now;
                return _autoMapper.Map<UserDto>(await _unitOfWork.UserRepository.Put(user));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al editar el usuario, UserService en el método UpdateUser, { ex.Message } ");
                throw new Exception($"{ ex.Message }");
            }
        }

        public  async Task<MekanoUser> GetUserByDocumentInMekano(string document)
        {
            return await _userRepository.GetUsersMekano()
                .Where(x => x.Document.Trim() == document.Trim() && x.Active == true).FirstOrDefaultAsync();
        }
    }
}
