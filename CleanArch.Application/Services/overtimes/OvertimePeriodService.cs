﻿using Application.Common.Mappings;
using Application.Core.Exceptions;
using Application.Cqrs.OvertimePeriods.Commands;
using Application.Cqrs.OvertimePeriods.Queries.GetPeriods;
using Application.DTOs.OvertimePeriods;
using Application.Interfaces.Overtimes;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.CustomEntities;
using Domain.Interfaces;
using Domain.Models.Overtimes;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services.Overtimes
{
    public class OvertimePeriodService : IOvertimePeriodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OvertimePeriodService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<PaginatedList<OvertimePeriodDto>> GetPeriods(GetOvertimePeriodQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.overtimePeriod
                    .Get()
                    .ProjectTo<OvertimePeriodDto>(_mapper.ConfigurationProvider)
                    .OrderByDescending(x => x.Id)
                    .PaginatedListAsync(request.PageNumber, request.PageSize);
       
        }


        public async Task<OvertimePeriod> PostPeriod(OvertimePeriod overtimePeriod ,CancellationToken cancellationToken)
        {
            return  await _unitOfWork.overtimePeriod.Add(overtimePeriod);

        }

        public async Task<OvertimePeriod> GetById(int Id)
        {
            return await _unitOfWork.overtimePeriod
                .Get()
                .FirstOrDefaultAsync(x => x.Id == Id);

        }
        public async Task<bool> checkById(int id)
        {
            var source = await _unitOfWork.overtimePeriod
                .Get().FirstOrDefaultAsync(x => x.Id == id);

            if (source == null || source.IsOpen == false) return false;

            return true;
        }

        public async Task<OvertimePeriod> PutPeriod(UpdateOvertimePeriodCommand request)
        {
            OvertimePeriod overtimePeriod = await GetById(request.Id)
                 ?? throw new NotFoundException("The overtime is not found");
            overtimePeriod = _mapper.Map(request, overtimePeriod);

            return await _unitOfWork.overtimePeriod.Put(overtimePeriod);
        }

    }
}
