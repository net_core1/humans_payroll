﻿using Application.Interfaces.Overtimes;
using Domain.Interfaces;
using Domain.Models.Overtimes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Application.Services.Overtimes
{
    public class OvertimeTypeService : IOvertimeTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OvertimeTypeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

      

        public async Task<IList<OvertimeType>> Get(CancellationToken cancellationToken)
        {
            return await _unitOfWork.overtimeType
                .Get()
                .ToListAsync(cancellationToken);

        }

        public async Task<bool> checkById(Guid id)
        {
            return await _unitOfWork.overtimeType
               .Get().CountAsync(x => x.Id == id) > 0 ? true : false;
        }

        public async Task<string> GetOvertimeTypeNameById(Guid id)
        {
            return await _unitOfWork.overtimeType.Get()
                                                 .Where(x => x.Id == id)
                                                 .Select(x => x.Name)
                                                 .FirstOrDefaultAsync();                                            
              
        }
    }
}
