﻿using Core.Models.configuration;
using Domain.Models.Mekano;
using Domain.Models.Overtimes;
using Domain.Models.Rol;
using System;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IIntRepository<OvertimePeriod> overtimePeriod { get; }
        IIntRepository<CostCenter> CostCenterRepository { get; }
        IIntRepository<MekanoUser> MekanoUserRepository { get; }
        IIntRepository<ResponsabilityCostCenter> responsabilityCostCenter { get; }
        IRepository<OvertimeDetail> overtimeDetail { get; }
        IRepository<OvertimeType> overtimeType { get; }
        IRepository<Configuration> ConfigurationRepository { get; }
        IRepository<Category> CategoryRepository { get; }
        IRepository<Models.User.User> UserRepository { get; }
        IRepository<Models.User.UserRol> UserRolRepository { get; }
        IRepository<Rol> RolRepository { get; }
        IRepository<OvertimeTemp> OvertimeTempRepository { get; }

        
        void SaveChanges();
        Task SaveChangesAsync();

        string GetDbConnection();
    }
}
