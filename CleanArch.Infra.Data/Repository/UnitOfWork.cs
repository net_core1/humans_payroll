﻿using CleanArch.Infra.Data.Context;
using Core.Models.configuration;
using Domain.Interfaces;
using Domain.Models.Mekano;
using Domain.Models.Overtimes;
using Domain.Models.Rol;
using Domain.Models.User;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly U27ApplicationDBContext _ctx;
        public IIntRepository<OvertimePeriod> overtimePeriod => new BaseIntRepository<OvertimePeriod>(_ctx);
        public IIntRepository<CostCenter> CostCenterRepository => new BaseIntRepository<CostCenter>(_ctx);
        public IIntRepository<MekanoUser> MekanoUserRepository => new BaseIntRepository<MekanoUser>(_ctx);
        public IIntRepository<ResponsabilityCostCenter> responsabilityCostCenter => new BaseIntRepository<ResponsabilityCostCenter>(_ctx);
        public IRepository<OvertimeDetail> overtimeDetail => new BaseRepository<OvertimeDetail>(_ctx);
        public IRepository<OvertimeType> overtimeType => new BaseRepository<OvertimeType>(_ctx);
        public IRepository<Category> CategoryRepository => new BaseRepository<Category>(_ctx);
        public IRepository<Configuration> ConfigurationRepository => new BaseRepository<Configuration>(_ctx);
        public IRepository<UserRol> UserRolRepository => new BaseRepository<UserRol>(_ctx);
        public IRepository<User> UserRepository =>  new BaseRepository<User>(_ctx);
        public IRepository<Rol> RolRepository =>  new BaseRepository<Rol>(_ctx);
        public IRepository<OvertimeTemp> OvertimeTempRepository =>  new BaseRepository<OvertimeTemp>(_ctx);


        public UnitOfWork(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
            
        }
        
        public string GetDbConnection()
        {
           return _ctx.Database.GetDbConnection().ConnectionString;
        }


        public void Dispose()
        {
            if (_ctx != null)
            {
                _ctx.Dispose();
            }
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }




        public async Task SaveChangesAsync()
        {
            await _ctx.SaveChangesAsync();
        }
    }
}
