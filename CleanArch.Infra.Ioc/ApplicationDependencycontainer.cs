﻿using Application.Common.BulkInserts;
using Application.Common.Interfaces;
using Application.Interfaces.Configurations;
using Application.Interfaces.Mekano;
using Application.Interfaces.Overtimes;
using Application.Interfaces.Rol;
using Application.Interfaces.User;
using Application.Services.Configurations;
using Application.Services.Mekano;
using Application.Services.overtimes;
using Application.Services.Overtimes;
using Application.Services.Rol;
using Application.Services.User;
using CleanArch.Application.Interfaces.Auths;
using CleanArch.Application.Services.Auths;
using Domain.Interfaces;
using Infra.Data.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Infra.Ioc
{
    public static class ApplicationDependencycontainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
         
            services.AddScoped<IBulkInsert, BulkInsert>();
            services.AddScoped<IUnitOfWork,UnitOfWork>();


            // Configuration
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<ICategoryService, CategoryService>();

            // overtime
            services.AddScoped<IOvertimeTypeService, OvertimeTypeService>();
            services.AddScoped<IOvertimePeriodService, OvertimePeriodService>();
            services.AddScoped<IOvertimeService, OvertimeService>();
           
           
            // Auth
            services.AddScoped<IAuthService, AuthService>();

            // User
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolService, RolService>();
            services.AddScoped<IUserRolService, UserRolService>();

            // CostCenter
            services.AddScoped<ICostCenterService, CostCenterService>();
            services.AddScoped<IResponsabilityCostCenterService, ResponsabilityCostCenterService>();

            // MekanoUser
            services.AddScoped<IMekanoUserService, MekanoUserService>();
            services.AddScoped<IOvertimeTempService, OvertimeTempService>();


        }

    }
}
